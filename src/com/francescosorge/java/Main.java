package com.francescosorge.java;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        if (OsUtils.isMac()) {
            System.out.println("Mac fa schifo. Lunga vita a Linux.");
        }

        ArrayList<String> files = new ArrayList<>(OsUtils.listFiles(".", ".db"));

        if (files.size() == 0) {
            System.out.println("Nessun database disponibile. Creandone uno nuovo.");
            MarksManager.createDatabase();
        } else {
            System.out.println("Seleziona un database da caricare.");
            MarksManager.loadDatabase(files);
        }

        mainMenu();
    }

    public static void mainMenu() {
        if (Integer.parseInt(MarksManager.user.get(2).get(0).toString()) != MarksManager.VERSION) {
            System.out.println("!!! ATTENZIONE !!!");
            System.out.println("La versione del database caricato non corrisponde alla versione richiesta da questo programma.");
            System.out.printf("Versione database: %d | Versione programma: %d \n", Integer.valueOf(MarksManager.user.get(2).get(0).toString()), MarksManager.VERSION);
            System.out.println("Si consiglia di uscire immediatamente e seguire la procedura illustrata su https://www.github.com/sorge13248/voti-its/migrate");
            System.out.print("Vuoi uscire? (y/n) ");
            String r = input.nextLine();
            if (r.isEmpty() || r.charAt(0) != 'n') {
                System.exit(401);
            }
        }

        System.out.println("Bentornato " + MarksManager.user.get(0).get(0) + " " + MarksManager.user.get(1).get(0));

        boolean exit = false;
        do {
            MarksManager.loadAll();

            if (MarksManager.marks.size() > 0) {
                float sum = 0;
                for (int i = 0; i < MarksManager.marks.size(); i++) {
                    sum += Float.parseFloat(MarksManager.marks.get(i).get(3).toString());
                }
                System.out.printf("La media dei tuoi voti è: %.2f\n", sum / MarksManager.marks.size());
            }

            System.out.println();
            System.out.println("1. Aggiungi voto");
            System.out.println("2. Stampa voti");
            System.out.println("3. Gestisci voti");
            System.out.println("4. Esci");
            System.out.print("Scegli una voce: ");

            byte r;
            try {
                r = input.nextByte();
            } catch(java.util.InputMismatchException e) { r = -2; }
            input.nextLine();

            System.out.println();

            if (r == 1) {
                MarksManager.addMark();
            } else if (r == 2) {
                MarksManager.printMarks(false);
            }  else if (r == 3) {
                MarksManager.manageMarks();
            } else if (r == 4) {
                System.out.println("Ciao ciao");
                MarksManager.closeDatabase();
                exit = true;
            } else {
                System.out.println("Comando sconosciuto");
            }
        } while(!exit);
    }

}

package com.francescosorge.java;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public final class MarksManager {
    public static final int VERSION = 2019_11_27_00;

    private static Sqlite database = null;
    static HashMap<Integer, ArrayList> marks = null;
    static HashMap<Integer, ArrayList> user = null;

    public static void loadDatabase(ArrayList<String> files) {
        System.out.printf("%d. %s\n", 0, "CREA UN NUOVO DATABASE");
        for (int i = 0; i < files.size(); i++) {
            System.out.printf("%d. %s\n", i+1, files.get(i).replace(".db", ""));
        }

        boolean exit = false;
        int i;

        do {
            System.out.print("Seleziona una voce indicandone il suo numero: ");
            try {
                i = Main.input.nextInt();
            } catch(java.util.InputMismatchException e) { i = -2; }
            Main.input.nextLine();

            if (i == 0) {
                exit = true;
                createDatabase();
            } else if (i > 0 && i < files.size()+1) {
                exit = true;
            } else {
                System.out.println("Scelta non valida");
            }
        } while(!exit);

        if (i != 0) {
            database = new Sqlite(files.get(i-1));
            loadAll();
        }
    }

    public static void closeDatabase() {
        database.close();
    }

    public static void loadAll() {
        marks = database.select("mark", new String[]{"id", "subject", "date", "mark"}, "date DESC");
        user = database.select("system", new String[]{"value"});
    }

    public static boolean deleteMark(int id) {
        if (database.delete("mark", new String[]{"id"}, new Sqlite.TYPES[]{Sqlite.TYPES.INTEGER}, new String[]{String.valueOf(id)})) {
            System.out.println("Eliminazione avvenuta con successo");
            return true;
        } else {
            System.out.println("Eliminazione NON avvenuta a causa di un errore");
            return false;
        }
    }

    public static void manageMarks() {
        printMarks(true);

        if (marks.size() > 0) {
            boolean exit = false;
            do {
                System.out.print("Scegli un voto (-1 per uscire): ");
                int i;
                try {
                    i = Main.input.nextInt();
                } catch (java.util.InputMismatchException e) {
                    i = -2;
                }
                Main.input.nextLine();

                if (i >= 0 && i < marks.size()) {
                    boolean exitSub = false;
                    do {
                        System.out.printf("Voto selezionato. Riga: %d, Materia: %s\n", i, marks.get(i).get(1));
                        System.out.println("Cosa vuoi farne?");
                        System.out.println("1. Elimina");
                        System.out.println("2. Escludi dalla media");
                        System.out.println("3. Esci");
                        System.out.print("Scegli una voce: ");
                        int choice;
                        try {
                            choice = Main.input.nextInt();
                        } catch (java.util.InputMismatchException e) {
                            choice = -2;
                        }
                        Main.input.nextLine();
                        System.out.println();

                        if (choice == 1) {
                            System.out.println("*** CONFERMA ELIMINAZIONE ***");
                            System.out.print("Scrivere y per confermare, qualsiasi altra cosa per annullare: ");
                            System.out.println();
                            String temp = Main.input.nextLine();
                            char c = temp.trim().isEmpty() ? 'n' : temp.charAt(0);

                            if (c == 'y') {
                                if (deleteMark(Integer.parseInt(marks.get(i).get(0).toString()))) {
                                    exitSub = true;
                                }
                            } else {
                                System.out.println("Eliminazione annullata");
                            }
                        } else if (choice == 2) {
                            System.out.println("Not implemented yet");
                        } else if (choice == 3) {
                            exitSub = true;
                        } else {
                            System.out.println("Selezione non valida");
                        }
                    } while (!exitSub);
                } else if (i == -1) {
                    exit = true;
                } else {
                    System.out.println("Selezione non valida");
                }
            } while (!exit);
        }
    }

    public static void printMarks(boolean rowNumbers) {
        if (marks.size() == 0) {
            System.out.println("Nessun voto da mostrare");
        } else {
            if (rowNumbers) System.out.printf("%-4s", "");
            System.out.printf("%-22s%-15s%-6s\n", "Materia", "Data", "Voto");
            for (int i = 0; i < marks.size(); i++) {
                if (rowNumbers) System.out.printf("%-4s", i);
                System.out.printf("%-22s%-15s%-6s\n", marks.get(i).get(1), marks.get(i).get(2), marks.get(i).get(3));
            }
        }
    }

    public static void addMark() {
        System.out.print("Nome materia: ");
        String subject = Main.input.nextLine();

        System.out.print("Data: ");
        String date = Main.input.nextLine();

        System.out.print("Voto: ");
        String mark = Main.input.nextLine();

        database.insert("mark", new String[]{"subject", "date", "mark"}, new Sqlite.TYPES[]{Sqlite.TYPES.STRING, Sqlite.TYPES.STRING, Sqlite.TYPES.INTEGER}, new String[]{subject, date, mark});

        System.out.println("Voto inserito");
    }

    public static void createDatabase() {
        boolean dbExists = false;

        System.out.print("Il tuo nome: ");
        String name = Main.input.nextLine();

        System.out.print("Il tuo cognome: ");
        String surname = Main.input.nextLine();

        do {
            String dbName = name + "-" + surname + ".db";
            System.out.printf("Nome file (lasciare vuoto per usare il nome predefinito: %s): ", dbName);
            String temp = Main.input.nextLine();
            if (!temp.trim().isEmpty()) {
                dbName = temp;
            }
            if (!dbName.endsWith(".db")) {
                dbName += ".db";
            }

            try {
                if (new File(dbName).exists()) {
                    System.out.println("Il database esiste già. Cambia nome o scegline un altro.");
                    dbExists = true;
                } else {
                    dbExists = false;

                    database = new Sqlite(dbName);

                    database.createTable("system", new String[]{"key TEXT PRIMARY KEY", "value TEXT"});
                    database.insert("system", new String[]{"key, value"}, new Sqlite.TYPES[]{Sqlite.TYPES.STRING, Sqlite.TYPES.STRING}, new String[]{"user_name", name});
                    database.insert("system", new String[]{"key, value"}, new Sqlite.TYPES[]{Sqlite.TYPES.STRING, Sqlite.TYPES.STRING}, new String[]{"user_surname", surname});
                    database.insert("system", new String[]{"key, value"}, new Sqlite.TYPES[]{Sqlite.TYPES.STRING, Sqlite.TYPES.INTEGER}, new String[]{"db_version", Integer.toString(VERSION)});

                    database.createTable("mark", new String[]{"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE", "subject TEXT NOT NULL", "date TEXT NOT NULL", "mark INTEGER NOT NULL", "avg INTEGER NOT NULL DEFAULT 1"});

                    System.out.println("Database creato.");
                }
            } catch(Exception e) {
                System.out.println("Errore fatale. Uscendo.");
                System.out.println(e.toString());
                System.exit(400);
            }
        } while(dbExists);

        loadAll();
    }
}

package com.francescosorge.java;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Sqlite {
    private String db = null;
    private Connection conn = null;

    public enum TYPES {
        STRING,
        INTEGER
    };

    public Sqlite(String db) {
        this.db = db;

        connect();
    }

    public boolean connect() {
        try {
            // db parameters
            String url = "jdbc:sqlite:" + this.db;
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean close() {
        if (conn != null) {
            try {
                conn.close();
                conn = null;
                return true;
            } catch(java.sql.SQLException e) {
                System.out.println(e.getMessage());
                return false;
            }
        }

        return false;
    }

    public boolean reconnect() {
        close();
        return connect();
    }

    public boolean createTable(String table, String[] fields) {
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS " + table + " (";

        for (int i = 0; i < fields.length; i++) {
            sql += fields[i];
            if (i != fields.length-1) {
                sql += ",";
            }
        }
        sql += ");";

        try (Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean insert(String table, String[] keys, TYPES[] types, String[] values) {
        String sql = "INSERT INTO " + table + "(";

        for (int i = 0; i < keys.length; i++) {
            sql += keys[i];
            if (i != keys.length-1) {
                sql += ",";
            }
        }
        sql += ") VALUES(";
        for (int i = 0; i < values.length; i++) {
            sql += "?";
            if (i != values.length-1) {
                sql += ",";
            }
        }
        sql += ");";

        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                if (types[i] == TYPES.INTEGER) {
                    pstmt.setInt(i + 1, Integer.parseInt(values[i]));
                } else if (types[i] == TYPES.STRING) {
                    pstmt.setString(i + 1, values[i]);
                }
            }
            pstmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public HashMap<Integer, ArrayList> select(String table, String[] fields) {
        return select(table, fields, null);
    }

    public HashMap<Integer, ArrayList> select(String table, String[] fields, String orderBy) {
        HashMap<Integer, ArrayList> temp = new HashMap<>();
        String sql = "SELECT ";

        for (int i = 0; i < fields.length; i++) {
            sql += fields[i];
            if (i != fields.length-1) {
                sql += ",";
            }
        }

        sql += " FROM " + table;

        if (orderBy != null) {
            sql += " ORDER BY " + orderBy;
        }

        try (Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){

            // loop through the result set
            int i = 0;
            while (rs.next()) {
                ArrayList<String> t = new ArrayList<>();

                for (int j = 0; j < fields.length; j++) {
                    t.add(rs.getString(fields[j]));
                }

                temp.put(i, t);
                i++;
            }

            return temp;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    public boolean delete(String table, String[] keys, TYPES[] types, String[] values) {
        String sql = "DELETE FROM " + table + " WHERE ";
        for (int i = 0; i < keys.length; i++) {
            sql += keys[i] + " = ?";
            if (i != keys.length-1) {
                sql += " AND ";
            }
        }

        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            // set the corresponding param
            for (int i = 0; i < keys.length; i++) {
                if (types[i] == TYPES.STRING) {
                    pstmt.setString(i + 1, values[i]);
                } else if (types[i] == TYPES.INTEGER) {
                    pstmt.setInt(i + 1, Integer.parseInt(values[i]));
                }
            }
            // execute the delete statement
            pstmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
}
